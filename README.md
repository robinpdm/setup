# Setup

Personal work setup.

![screenshot](./assets/screenshot.png)

## Operating System

[macOS 15 (Sequoia)](https://www.apple.com/macos/macos-sequoia/)

## Desktop

| Name                                                                   | Description                                                              |
| ---------------------------------------------------------------------- | ------------------------------------------------------------------------ |
| [Rectangle](https://rectangleapp.com/)                                 | Move and resize windows in macOS using keyboard shortcuts or snap areas. |
| [Stats](https://github.com/exelban/stats)                              | Open-source system monitoring widgets.                                   |
| [Clocker](https://github.com/n0shake/clocker)                          | macOS app to plan and organize through timezones.                        |

## Terminal

| Name                                                                   | Description                                                              | Configuration                                                            |
| ---------------------------------------------------------------------- | ------------------------------------------------------------------------ | ------------------------------------------------------------------------ |
| [iTerm2](https://iterm2.com/)                                          | Terminal emulator for macOS that does amazing things.                    | See [profile](./iterm-2)                                                 |
| [Zsh](https://en.wikipedia.org/wiki/Z_shell)                           | Shell designed for interactive use.                                      | See [configuration](./.zshrc)                                            |
| [Starship](https://starship.rs/)                                       | Minimal, blazing-fast, and infinitely customizable prompt.               | -                                                                        |

### Utilities

| Name                                                                   | Description                                                              |
| ---------------------------------------------------------------------- | ------------------------------------------------------------------------ |
| [thefuck](https://github.com/nvbn/thefuck)                             | Magnificent app which corrects your previous console command.            |
| [tree](https://formulae.brew.sh/formula/tree)                          | A recursive directory listing command.                                   |
| [dyff](https://github.com/homeport/dyff)                               | /ˈdʏf/ - diff tool for YAML files, and sometimes JSON.                   |
| [turbogit](https://github.com/b4nst/turbogit)                          | Opinionated CLI enforcing clean Git workflow without comprising UX.      |
| [sops](https://github.com/mozilla/sops)                                | Simple and flexible tool for managing secrets.                           |
| [lazydocker](https://github.com/jesseduffield/lazydocker)              | The lazier way to manage everything docker.                              |
| [k9s](https://github.com/derailed/k9s)                                 | Kubernetes CLI to manage your clusters in style!                         |
| [kubectx](https://github.com/ahmetb/kubectx)                           | Faster way to switch between clusters and namespaces in kubectl.         |

## Code Editor

### Visual Studio Code

#### Extensions
| Name                                                                                                                   | Description                                                                                                                                      |
| ---------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| [Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack) | An extension pack that lets you open any folder in a container, on a remote machine, or in WSL and take advantage of VS Code's full feature set. |
| [GitHub Theme](https://marketplace.visualstudio.com/items?itemName=GitHub.github-vscode-theme)                         | GitHub theme for VS Code.                                                                                                                        |
| [GitLens - Git supercharged](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)                      | Supercharge Git within VS Code.                                                                                                                  |
| [Git Graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph)                                    | View a Git Graph of your repository, and perform Git actions from the graph.                                                                     |
| [GitHub Copilot](https://marketplace.visualstudio.com/items?itemName=GitHub.copilot)                                   | Your AI pair programmer.                                                                                                                         |
| [Terminal Capture](https://marketplace.visualstudio.com/items?itemName=devwright.vscode-terminal-capture)              | Take an open terminal's output and open it in a tab to quickly navigate and edit the output.                                                     |
| [change-case](https://marketplace.visualstudio.com/items?itemName=wmaurer.change-case)                                 | Quickly change the case (camelCase, CONSTANT_CASE, snake_case, etc) of the current selection or current word.                                    |
| [vscode-sops](https://marketplace.visualstudio.com/items?itemName=signageos.signageos-vscode-sops)                     | Integration of SOPS by Mozilla into VS Code.                                                                                                     |
| [DotENV](https://marketplace.visualstudio.com/items?itemName=mikestead.dotenv)                                         | Support for dotenv file syntax.                                                                                                                  |
| [GraphQL](https://marketplace.visualstudio.com/items?itemName=GraphQL.vscode-graphql)                                  | GraphQL extension for VS Code: adds syntax highlighting, validation, and language features.                                                      |
| [VS Code Jupyter Notebook Previewer](https://marketplace.visualstudio.com/items?itemName=jithurjacob.nbpreviewer)      | An easy to use extension for previewing Jupyter Notebooks within VS Code.                                                                        |
| [Markdown Preview Enhanced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced)   | Markdown Preview Enhanced ported to VS Code.                                                                                                     |

### Other

| Name                                                                   | Description                                                              |
| ---------------------------------------------------------------------- | ------------------------------------------------------------------------ |
| [Altair GraphQL Client](https://github.com/altair-graphql/altair)      | A beautiful feature-rich GraphQL Client for all platforms.               |
