######################################################################################################################################################

export EDITOR='code --wait'

setopt HIST_IGNORE_SPACE

HISTFILE=~/.zsh_history
HISTSIZE=999999999
SAVEHIST=${HISTSIZE}

export CORRECT_IGNORE_FILE='.*'

######################################################################################################################################################

# Shell

## Starship

eval "$(starship init zsh)"

# History search
autoload -U history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
bindkey "^[[A" history-beginning-search-backward-end
bindkey "^[[B" history-beginning-search-forward-end

# Completion
autoload -Uz compinit && compinit
fpath=(~/.zsh $fpath)

# Git completion
zstyle ':completion:*:*:git:*' script ~/.zsh/git-completion.bash

# Kubectl completion
source <(kubectl completion zsh)

# Color completion for some things
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

# Select with arrow keys
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' list-colors ''
zstyle ':completion:*:*:*:*:*' menu select

# Faster completion
zstyle ':completion::complete:*' use-cache 1

# Add `/` as delimiter for word navigation
# https://stackoverflow.com/questions/60729605/word-forward-backward-delimiter-difference-between-bash-and-zsh
export WORDCHARS="${WORDCHARS/\//}"

######################################################################################################################################################

# Utilities

## thefuck

eval $(thefuck --alias)

## Git

alias gc="git checkout"
alias gcb="git checkout -b"
alias gr="git rebase"
alias gs="git stash"
alias gsp="git stash pop"

# Checkout default branch
alias gcm="git checkout $(git branch | cut -c 3- | grep -E '^master$|^main$')"

# Prune local tracking branches that do not exist on remote anymore
# https://stackoverflow.com/questions/13064613/how-to-prune-local-tracking-branches-that-do-not-exist-on-remote-anymore/17029936#17029936
alias gcx="git fetch -p ; git branch -r | awk '{print \$1}' | egrep -v -f /dev/fd/0 <(git branch -vv | grep origin) | awk '{print \$1}' | xargs git branch -D"

## Docker

alias dcp="yes | docker container prune"
alias dvp="yes | docker volume prune"
alias dnp="yes | docker network prune"
alias dsp="yes | docker system prune"

# Remove dangling images
alias drmi="docker rmi $(docker images -f dangling=true -q)"

## VS Code

export PATH="$PATH:/Applications/Visual Studio Code.app/Contents/Resources/app/bin"

## Go

export GOPATH=$HOME/go
export GOROOT=/usr/local/go
export GOBIN=$GOPATH/bin
export PATH=$PATH:$GOPATH
export PATH=$PATH:$GOROOT/bin
export PATH=$PATH:$GOBIN

######################################################################################################################################################
